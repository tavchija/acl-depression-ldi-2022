## Get all predictions
from collections import Counter
import operator
import glob
import os
import tarfile
import json
from sklearn.metrics import classification_report, accuracy_score


def get_all_pred_files(rfolder):
    """
    A method which traverses the cluster results and gathers (loads) the predictions.
    """

    full_predictions = {}
    full_names = {}
    all_files = list(glob.glob(rfolder))

    for enx, filex in enumerate(all_files):

        try:
            pfile = os.path.join(filex, "predictions.tar.gz")
            tar = tarfile.open(pfile, encoding="utf-8")

            predictions = []
            for x in tar.getmembers():
                flx = tar.extractfile(x)
                try:
                    r = flx.read().decode("utf-8")

                    r = json.loads(r)
                    predictions.append(r)
                except Exception:
                    pass

            if len(predictions) == 0:
                continue
            if len(predictions) >= 1:

                for el in predictions:
                    if 'model_specification' in el:
                        identifier = int(enx**(len(predictions)))
                        el['result_file'] = filex
                        full_predictions[identifier] = el
                        try:

                            config = el['model_specification']
                            id_keys = [
                                'GLOBAL_LEARNER_PRESET',
                                'GLOBAL_CONFIG_REPTYPE', 'GLOBAL_RANDOM_SEED',
                                'GLOBAL_CONFIG_TIME'
                            ]

                            gname = []

                            for key in id_keys:
                                gname.append(str(config[key]))
                            
                            full_names[identifier] = "--".join(gname)

                        except Exception as es:
                            print(es)
                            full_names[identifier] = "test"
        except Exception:
            pass

    print(f"Obtained {len(full_predictions)} predictions.")
    return full_predictions, full_names


def write_separate_predictions(outfolder, predictions, names):

    for k, v in predictions.items():
        full_name = names[k]
        with open(outfolder + f"test_{k}_" + f"{full_name}_report.json",
                  "w") as outf:
            json.dump(v, outf)


def generate_an_ensemble_prediction(outfolder, predictions):

    total_predictions = []
    total_real = []
    plist = list(predictions.keys())
    for j in range(len(predictions[plist[0]]['predictions'])):
        all_predictions_instance = []
        for k, v in predictions.items():

            prediction = v['predictions'][j]
            all_predictions_instance.append(prediction)

        cnts = sorted(dict(Counter(all_predictions_instance)),
                      key=operator.itemgetter(1),
                      reverse=True)
        mode_pred = cnts[0]
        total_predictions.append(mode_pred)
        total_real.append(v['real'][j])
    out_struct = {"predictions": total_predictions, "real": total_real}
    k = 1
    report_classification = classification_report(total_predictions,
                                                  total_real,
                                                  output_dict=True)
    report_classification['real'] = total_real
    report_classification['predictions'] = total_predictions
    report_classification['accuracy'] = accuracy_score(total_predictions,
                                                       total_real)
    with open(outfolder + f"test_{k}_" + f"grid-ensemble{k}_report.json",
              "w") as outf:
        json.dump(report_classification, outf)


if __name__ == "__main__":

    rfolder = "results/*"
    outfolder = "predictions/"
    all_predictions, all_names = get_all_pred_files(rfolder)
    write_separate_predictions(outfolder, all_predictions, all_names)
