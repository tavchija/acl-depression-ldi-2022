## autoBOT - cluster execution

This repository contains the basic machinery to scale autoBOT-based learners to a computing grid.
It assumes Singularity-container-based execution, and cache on the cluster.

## Building the container
in `containers` folder, simply

```
bash generate.sh
```

## Configuring jobs

The file `global_config.py` includes parameter settings for all main hyperparameters specifying the scaling to a given cluster.
Simply change the properties there, replace the data in the `data` folder and you should be good to go.

## First prepare the folder structure

```
bash clean_and_prepare.sh
```

## Run the full autoML process on the grid

The first argument is the proxy server, the second one is the cluster ID.
```
bash run_everything.sh gen.vo.sling.si trebuchet.ijs.si;nsc.ijs.si
```
Note the `;`-separated list of clusters. The job scheduler randomly sends the jobs to either of the clusters to reduce the workload.

## How does job scheduling work?
Based on `global_config.py` the main job engine decides how often and where the jobs are sent to. Everywhere (each cluster), however, Singularity needs to be installed; and dCache accessible.