## Clean all current jobs
for j in current_jobs/*;
do
    arcclean -i $j;
done

## Clean any old files if present
rm -rvf current_jobs;
rm -rf results;
rm -rf predictions;
rm -rf cluster_xrsl;
rm -rf raw_jobs;

## Re-generate the folder structure
mkdir results; mkdir predictions; mkdir current_jobs; mkdir models; mkdir final_models
echo "Cleanup successful! You can run the full search now."
