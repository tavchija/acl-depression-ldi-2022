BootStrap: docker
From: ubuntu:latest

%labels

%environment
export LC_ALL=C

%post
export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install -y libhdf5-dev graphviz locales python3-dev python3-pip curl git zip
apt-get -y install nordugrid-arc-client nordugrid-arc-plugins-needed nordugrid-arc-plugins-globus
apt-get clean

pip3 install autoBOTLib
pip3 install emoji
python3 -m nltk.downloader punkt
python3 -m nltk.downloader stopwords
pip3 install sentence_transformers

unset DEBIAN_FRONTEND