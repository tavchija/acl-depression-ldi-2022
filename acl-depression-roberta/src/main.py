from json import load
from numpy.random.mtrand import random
import pandas as pd
import numpy as np
from nlp import Dataset
from nltk.tokenize import word_tokenize
import tensorflow as tf
from sklearn.utils import shuffle
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoTokenizer
from datasets import load_dataset, load_metric
from transformers import TrainingArguments
from transformers import Trainer
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import DataCollatorWithPadding
from transformers.file_utils import PaddingStrategy
from transformers.tokenization_utils_base import TruncationStrategy
from sklearn.model_selection import train_test_split
from transformers import RobertaConfig, RobertaModel
from transformers import RobertaTokenizer
from datasets import load_metric
from collections import Counter
import nltk.tokenize
import os
os.environ['CUDA_VISIBLE_DEVICES']="4"


tokenizer = AutoTokenizer.from_pretrained("roberta-base")
def preprocess_function(examples):
    return tokenizer(examples["text"],truncation=True)

    
# Data
train_df = pd.read_csv("./data/train.tsv", "\t")
test_df = pd.read_csv("./data/test.tsv", "\t")
real_test = pd.read_csv("./data/test_data.tsv", "\t")








preds = pd.DataFrame()
preds["test_id (pid)"] = real_test["Pid"]
#print(real_test.columns)


real_test = real_test.rename(columns={"text data" : "text"})
train_df = train_df.rename(columns={"text_a" : "text"})
test_df = test_df.rename(columns={"text_a" : "text"})


#concatenate dataframe
#train_df = pd.concat([train_df, test_df], axis=0)


train_ds = Dataset.from_pandas(train_df[["PID", "text", "label"]])
test_ds = Dataset.from_pandas(test_df[["PID", "text"]])
#tokenize
tokenized_train = train_ds.map(preprocess_function)
tokenized_test = test_ds.map(preprocess_function)

#print(tokenized_train['input_ids'])


tokenized_train.drop('text')
tokenized_train.drop('PID')
tokenized_train.drop('__index_level_0__')
tokenized_test.drop('text')
tokenized_test.drop('PID')
#tokenized_test.drop('__index_level_0__')

data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

model = AutoModelForSequenceClassification.from_pretrained("roberta-base", num_labels=3)
training_args = TrainingArguments(
    output_dir='./results',
    per_device_train_batch_size=32,
    num_train_epochs=10,
)
# Trainer object initialization
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=tokenized_train,
    tokenizer=tokenizer,
    data_collator=data_collator,
)



trainer.train()
num = int(random() * 10000)
trainer.save_model(f"./models/roberta.pickle")

pred = trainer.predict(tokenized_test)
model_predictions = np.argmax(pred[0], axis=1)
"""
preds["class_label"] = pd.Series(dtype="str")
for i in range(len(model_predictions)):
    if model_predictions[i] == 2:
        preds["class_label"][i] = "severe"
    elif model_predictions[i] == 1:
        preds["class_label"][i] = "moderate"
    else:
        preds["class_label"][i] = "not depression"

preds.to_csv("./data/E8@IJS_RoBERTafirstrun.tsv", index=False, sep="\t")
"""



metric = load_metric("accuracy")
metric_f1 = load_metric("f1")

final_score = metric.compute(predictions=model_predictions, references=tokenized_test['label'])
print(f"Final test accuracy: {final_score}")
final_score = metric_f1.compute(predictions=model_predictions, references=tokenized_test['label'], average="macro")

print(f"Final test f1-macro: {final_score}")

final_score = metric_f1.compute(predictions=model_predictions, references=tokenized_test['label'], average="micro")

print(f"Final test f1-micro: {final_score}")


train_labels = tokenized_test['label']
counter_labels_train = Counter(train_labels)
most_frequent_label = counter_labels_train.most_common(1)[0][0]


model_predictions = np.repeat(most_frequent_label, len(model_predictions))
majority_score = metric.compute(predictions=model_predictions, references=tokenized_test['label'])
print(f"Majority classifier (label=3): {majority_score}")

