import pandas as pd
import autoBOTLib
import secrets


if __name__ == "__main__":
    test = pd.read_csv("./data/test_data(1).tsv", sep="\t")

    test_sequences = test['text data'].values.tolist()
    test_pid = test["Pid"].values.tolist()
    
    predictions = pd.read_csv("./data/pred.csv")

    res = pd.DataFrame()

    list_lab = []

    
    for i in range(len(predictions)):
        a = predictions["severe"][i]
        b = predictions["moderate"][i]
        c = predictions["not depression"][i]
        if a >= b and a >= c:
            list_lab.append("severe")
        elif b >= a and b >= c:
            list_lab.append("moderate")
        else: 
            list_lab.append("not depression")
    res["test_id (pid)"] = test_pid
    res["class_label"] = list_lab

    res.to_csv("./data/E8@IJS_AutoBOTsecondrun.tsv", sep="\t", index=False)

    print(predictions)
